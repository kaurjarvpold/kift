var express = require('express');
var app = express();
var path = require('path');

app.use(express.static('frontend/dist'))

// viewed at http://localhost:8080
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/frontend/dist/index.html'));
});

app.listen(80, function() {
	console.log('Listening at port 80');
	console.log('Listening at port 80');
});