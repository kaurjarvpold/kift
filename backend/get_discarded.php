<?php
header('Access-Control-Allow-Origin: *');
include 'connect_db.php';

$parties = mysqli_query($con, "SELECT * FROM discarded WHERE end_time > NOW() ORDER BY start_time ASC");
$result = array();

if (mysqli_num_rows($parties) > 0) {

    while($row = mysqli_fetch_array($parties)) {
      $id = $row['id'];
      $event_name = $row['event_name'];
      $description = $row['description'];
      $starttime = $row['start_time'];
      $starttime = new DateTime($starttime);
      $starttime = $starttime->format('d/m/Y H:i');
      $endtime = $row['end_time'];
      $endtime = new DateTime($endtime);
      $endtime = $endtime->format('d/m/Y H:i');
      $venue_name = $row['venue_name'];
      $latitude = $row['latitude'];
      $longitude = $row['longitude'];
      $street_address = $row['street_address'];
      $zip = $row['zip'];
      $picture = $row['picture'];
      $city = $row['city'];

      $result[] = array('id'=> $id, 'event_name'=> $event_name, 'description'=> $description, 'starttime'=> $starttime,
       'endtime'=> $endtime, 'venue_name'=> $venue_name, 'latitude'=> $latitude, 'longitude'=> $longitude, 'street_address'=> $street_address,
       'zip'=> $zip, 'picture'=> $picture, 'city'=> $city);
    }
} else {
    return "null";
}

echo json_encode($result);

 ?>
