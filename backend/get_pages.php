<?php
header('Access-Control-Allow-Origin: *');
include '../backend/connect_db.php';

$pages = mysqli_query($con, "SELECT * FROM page ORDER BY id DESC");
$result = array();

if (mysqli_num_rows($pages) > 0) {

    while($row = mysqli_fetch_array($pages)) {
      $id = $row['id'];
      $url = $row['url'];
      $name =  $row['name'];
      $type = $row['type'];
      $result[] = array('id'=> $id, 'url' => $url, 'type' => $type, 'name' => $name);
    }

} else {

    $result[] =  "null";
}

echo json_encode($result);

 ?>
